// import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/css/style.css'
//import './assets/theme/theme-green/index.css'
import store from './vuex/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
// import Mock from './mock'
import router from '@/router';
import myFilter from '@/filters/index.js' // 引入filter
import * as load from '@/utils/loading.js'

import { getCookie } from '@/utils/util.js';

// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'
import '@/assets/css/mystyle.css'

import Viewer from "v-viewer";
import "viewerjs/dist/viewer.css";

Vue.use(ElementUI)
Vue.use(Vuex)

Vue.use(Viewer);
Viewer.setDefaults({
  Options: { 
    "inline": true,
    "button": true, 
    "navbar": true,
    "title": true,
    "toolbar": true,
    "tooltip": true,
    "movable": true,
    "zoomable": true,
    "rotatable": true, 
    "scalable": true, 
    "transition": true, 
    "fullscreen": true, 
    "keyboard": true, 
    "url": "data-src" 
  }
});

Vue.prototype.$load = load;

//NProgress.configure({ showSpinner: false });

// 挂载filter到全局filter
for (let key in myFilter) {
  Vue.filter(key, myFilter[key])
}

router.beforeEach((to, from, next) => {
  //NProgress.start();
  if (to.path == '/login') {
    sessionStorage.removeItem('user');
  }
  let token = getCookie('token');
  if (token === '' && to.path != '/login') {
    next({ path: '/login' })
  } else {
    next()
  }
})

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
  //el: '#app',
  //template: '<App/>',
  router,
  store,
  //components: { App }
  render: h => h(App)
}).$mount('#app')

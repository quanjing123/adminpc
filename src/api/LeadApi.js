import request from '@/utils/request';
const Qs = require('qs');

// 文章分类
export const articleTypeListApi = params => {
  return request({
    url: '/article/type',
    method: 'post',
    data: params
  });
};
export const articleTypeSaveApi = params => {
  return request({
    url: '/article/type/save',
    method: 'post',
    data: params
  });
};
export const articleTypeDeleteApi = params => {
  return request({
    url: '/article/type/delete',
    method: 'post',
    data: params
  });
};
export const articleTypeSelectApi = params => {
  return request({
    url: '/article/type/select',
    method: 'post',
    data: params
  });
};


// 文章
export const articleListApi = params => {
  return request({
    url: '/article',
    method: 'post',
    data: params
  });
};
export const articleSaveApi = params => {
  return request({
    url: '/article/save',
    method: 'post',
    data: params
  });
};
export const articleDetailApi = params => {
  return request({
    url: '/article/detail',
    method: 'post',
    data: params
  });
};
export const articleStatusSaveApi = params => {
  return request({
    url: '/article/status/save',
    method: 'post',
    data: params
  });
};
export const articlePositionSaveApi = params => {
  return request({
    url: '/article/position/save',
    method: 'post',
    data: params
  });
};
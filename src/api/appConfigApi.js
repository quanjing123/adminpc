import {
    get,
    post
} from '@/utils/request2';

//列表
export const appConfigGoodsListApi = params => get('/app/config/goods', params);
export const appConfigGoodsEditApi = params => post('/app/config/goods/edit', params, true);
export const appConfigGoodsRemoveApi = params => post('/app/config/goods/remove', params, true);
import {
  get,
  post
} from '@/utils/request2';


// Banner列表
export const listApi = params => get('/app/config/banner', params);
// Banner新增或修改
export const editApi = params => post('/app/config/banner/edit', params, true);
// Banner删除
export const removeApi = params => post('/app/config/banner/remove', params, true);
import request from '@/utils/request';
import {
  get,
  post
} from '@/utils/request2';

const Qs = require('qs');

// 列表
export const tempalteListApi = params => {
  return request({
    url: '/template',
    method: 'get',
    params: params
  });
};

// 新增或修改
export const templateSaveApi = params => {
  return request({
    url: '/template/save',
    method: 'post',
    data: params
  });
};

// 发布或下架
export const templateUpdateApi = params => {
  return request({
    url: '/template/update',
    method: 'post',
    data: params
  });
};

// 预览
export const templatePreviewApi = params => {
  return request({
    url: '/template/preview',
    method: 'post',
    data: params
  });
};

// 详情
export const templateDetailApi = params => {
  return request({
    url: '/template/detail',
    method: 'post',
    data: params
  });
};


export const templateRemoveApi = params => post('/template/remove', params, true);
import {
    get,
    post
} from '@/utils/request2';

//订单
export const ticketOrderListApi = params => get('/ticket/order', params);
export const ticketOrderAuditPassListApi = params => post('/ticket/order/audit/pass', params, true);
export const ticketOrderAuditRefuseApi = params => post('/ticket/order/audit/refuse', params, true);
export const ticketOrderMemoApi = params => post('/ticket/order/memo', params, true);

//奖券
export const ticketListApi = params => get('/ticket', params);
export const ticketListDownloadApi = params => get('/ticket/download', params, {
    isDownload: true
});
export const ticketDetailOrderApi = params => get('/ticket/detail/order', params);
export const ticketDetailOrderDownloadApi = params => get('/ticket/detail/order/download', params, {
    isDownload: true
});

//核销
export const ticketInfoApi = params => post('/ticket/info', params);
export const ticketUseApi = params => post('/ticket/use', params, true);

//订单统计
export const ticketOrderCompanyApi = params => get('/ticket/order/company', params);
export const ticketOrderCompanyDownloadApi = params => get('/ticket/order/company/download', params, {
    isDownload: true
});
export const ticketOrderCompanyDetailApi = params => get('/ticket/order/company/detail', params);
export const ticketOrderCompanyDetailDownloadApi = params => get('/ticket/order/company/detail/download', params, {
    isDownload: true
});
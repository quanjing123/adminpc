import {
    get,
    post
} from '@/utils/request2';

//订单
export const dynamicListApi = params => get('/dynamic/list', params);
export const dynamicRemoveApi = params => post('/dynamic/remove', params, true);

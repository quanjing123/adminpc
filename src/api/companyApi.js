import request from '@/utils/request';
import {
  get,
  post
} from '@/utils/request2';
const Qs = require('qs');

// 企业下拉列表
export const companyListSelectApi = params => {
  return request({
    url: '/company/list/select',
    method: 'post',
    params: params
  });
};

// 设备审核
export const equAudit = params => {
  return request({
    url: '/equ/audit',
    method: 'post',
    data: params
  });
};
// 设备删除
export const equDelete = params => {
  return request({
    url: '/equ/delete',
    method: 'post',
    data: params
  });
};
// 设备审核列表
export const equList = params => {
  return request({
    url: '/equ/index',
    method: 'get',
    params: params
  });
};

// 企业产品列表
export const companyGoodsList = params => {
  return request({
    url: '/company/goods/list',
    method: 'post',
    data: params
  });
}; // 企业产品编辑
export const companyGoodsUpdate = params => {
  return request({
    url: '/company/goods/update',
    method: 'post',
    data: params
  });
}; // 企业产品规格编辑
export const companyGoodsSpecUpdate = params => {
  return request({
    url: '/company/goods/spec/update',
    method: 'post',
    data: params
  });
};


// 企业类目删除
export const companyApplyCategoryDelete = params => {
  return request({
    url: '/company/apply/category/delete',
    method: 'post',
    data: params
  });
};
// 企业类目新增
export const companyApplyCategoryAdd = params => {
  return request({
    url: '/company/apply/category/add',
    method: 'post',
    data: params
  });
};
// 企业类目列表
export const companyApplyCategoryList = params => {
  return request({
    url: '/company/apply/category/list',
    method: 'post',
    data: params
  });
};
// 企业申请状态编辑
export const companyApplyUpdateState = params => {
  return request({
    url: '/company/apply/updateState',
    method: 'post',
    data: params
  });
};
// 企业申请列表
export const companyApplyList = params => {
  return request({
    url: '/company/apply/list',
    method: 'post',
    data: params
  });
};

export const companyCategoryList = params => {
  return request({
    url: '/company/category/list',
    method: 'post',
    data: params
  });
};

export const companyCategoryDel = params => {
  return request({
    url: '/company/category/delete',
    method: 'post',
    data: params
  });
};

export const companyCategoryAdd = params => {
  return request({
    url: '/company/category/add',
    method: 'post',
    data: params
  });
};

export const goodsTypeSelectList = params => {
  return request({
    url: '/goods/type/select',
    method: 'post',
    data: params
  });
};


// 企业平台
export const companyPlatformListApi = params => {
  return request({
    url: '/company/platform/list',
    method: 'post',
    data: params
  });
};
export const companyPlatformAddApi = params => {
  return request({
    url: '/company/platform/add',
    method: 'post',
    data: params
  });
};
export const companyPlatformAddChildApi = params => {
  return request({
    url: '/company/platform/add/child',
    method: 'post',
    data: params
  });
};
export const companyPlatformRemoveApi = params => {
  return request({
    url: '/company/platform/remove',
    method: 'post',
    data: params
  });
};
export const companySuncodeDownloadApi = params => {
  return request({
    url: '/company/suncode',
    method: 'post',
    params: params,
    responseType: 'arraybuffer'
  });
};

// 企业推送权限
export const companyContentListApi = params => {
  return request({
    url: '/company/content/list',
    method: 'post',
    params: params
  });
};
export const companyContentLabelApi = params => {
  return request({
    url: '/company/content/label',
    method: 'post',
    data: params
  });
};


// 企业执照审核
export const companyLicenseListApi = params => {
  return request({
    url: '/company/license/list',
    method: 'post',
    data: params
  });
};
export const companyLicenseAuditApi = params => {
  return request({
    url: '/company/license/audit',
    method: 'post',
    data: params
  });
};
export const companyLicenseUpdateApi = params => post('/company/license/update', params, true);
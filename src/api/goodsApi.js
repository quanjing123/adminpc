import request from '@/utils/request';
const Qs = require('qs');

// 下拉列表
export const getGoodsSelectListApi = params => {
  return request({
    url: '/goods/select',
    method: 'get',
    params: params
  });
};
// 下拉列表-经销APP配置
export const getGoodsSelect2ListApi = params => {
  return request({
    url: '/goods/select2',
    method: 'get',
    params: params
  });
};


// 商品批量导入
export const goodsCopyApi = params => {
  return request({
    url: '/goods/copy',
    method: 'post',
    params: params
  });
};


// 商品平台分类
export const goodsTypeListApi = params => {
  return request({
    url: '/goods/type/list',
    method: 'post',
    data: params
  });
};
export const goodsTypeAddApi = params => {
  return request({
    url: '/goods/type/add',
    method: 'post',
    data: params
  });
};
export const goodsTypeDeleteApi = params => {
  return request({
    url: '/goods/type/delete',
    method: 'post',
    data: params
  });
};
export const goodsTypeMoveApi = params => {
  return request({
    url: '/goods/type/move',
    method: 'post',
    data: params
  });
};
export const goodsTypeParentApi = params => {
  return request({
    url: '/goods/type/parent',
    method: 'post',
    data: params
  });
};

//商品管理
export const goodsListApi = params => {
  return request({
    url: '/goods/list',
    method: 'post',
    data: params
  });
};
export const goodsEditApi = params => {
  return request({
    url: '/goods/platform/type/edit',
    method: 'post',
    data: params
  });
};
export const goodsRemoveApi = params => {
  return request({
    url: '/goods/platform/type/remove',
    method: 'post',
    data: params
  });
};

export const editPopularityApi = params => {
  return request({
    url: '/goods/popularity/add',
    method: 'post',
    data: params
  });
};



import {
  get,
  post
} from '@/utils/request2';
//订单统计
export const goodsGroupBuyingListApi = params => get('/goods/group/list', params);
export const goodsGroupBuyingInfoApi = params => get('/goods/group/info', params);
export const goodsGroupBuyingSaveApi = params => post('/goods/group/save', params, true);
export const goodsGroupBuyingUpApi = params => post('/goods/group/up', params, true);
export const goodsGroupBuyingDownApi = params => post('/goods/group/down', params, true);
export const goodsGroupBuyingRemoveApi = params => post('/goods/group/remove', params, true);
import request from '@/utils/request';
const Qs = require('qs');

// 申请列表
export const goodsSelectListApi = params => {
    return request({
        url: '/company/select',
        method: 'get',
        params: params
    });
};

// 开通
export const goodsSelectAgreeApi = params => {
    return request({
        url: '/company/select/agree',
        method: 'post',
        params: params
    });
};

// 拒绝
export const goodsSelectRefuseApi = params => {
    return request({
        url: '/company/select/refuse',
        method: 'post',
        params: params
    });
};

// 商品批量导入
export const goodsCopyApi = params => {
    return request({
        url: '/company/select/agree',
        method: 'post',
        params: params
    });
};
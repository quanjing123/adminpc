import request from '@/utils/request';
const Qs = require('qs');

// 列表
export const labelListApi = params => {
    return request({
        url: '/label',
        method: 'post',
        data: params
    });
};

// 新增或修改
export const labelUpdateApi = params => {
    return request({
        url: '/label/save',
        method: 'post',
        data: params
    });
};

// 删除
export const labelDeleteApi = params => {
    return request({
        url: '/label/delete',
        method: 'post',
        data: params
    });
};

// 企业编辑标签列表
export const labelCompanyListApi = params => {
    return request({
        url: '/label/company',
        method: 'post',
        data: params
    });
};

// 企业标签更新
export const labelCompanySaveApi = params => {
    return request({
        url: '/label/company/save',
        method: 'post',
        data: params
    });
};
import {
    get,
    post
} from '@/utils/request2';

//订单
export const liveListApi = params => get('/live/list', params);
export const liveRemoveApi = params => post('/live/remove', params, true);
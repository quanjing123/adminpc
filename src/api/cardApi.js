import {
    get,
    post
} from '@/utils/request2';

//详情
export const cardInfoApi = params => get('/businessCard/info', params);

export const cardAuthSaveApi = params => post('/businessCard/auth/save', params, true);
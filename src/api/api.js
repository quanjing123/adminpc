import request from '@/utils/request';
const Qs = require('qs');

// /adminHome/login 登录
export const loginApi = params => {
    return request({
        url: '/home/login',
        transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
          data = Qs.stringify(data);
          return data
        }],
        method: 'post',
        data: params,
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded'
        },
    });
};

// /index/dataCenter 企业首页数据概况
export const dataCenterApi = params => {
  return request({
      url: 'index/dataCenter',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /index/indexData 企业首页数据统计
export const indexDataApi = params => {
  return request({
      url: 'index/indexData',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /index/newCountList 企业首页数据统计折线图
export const newCountListApi = params => {
  return request({
      url: 'index/newCountList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /index/newUserCountList 数据中心-用户增长情况
export const newUserCountListApi = params => {
  return request({
      url: 'index/newUserCountList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /index/userUseCountList 数据中心-使用人数增长情况
export const userUseCountListApi = params => {
  return request({
      url: 'index/userUseCountList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /index/userSexCount 用户画像-性别分布
export const userSexCountApi = params => {
  return request({
      url: 'index/userSexCount',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /businessCard/selectCustomerList 查询用户列表
export const selectCustomerListApi = params => {
  return request({
      url: 'businessCard/selectCustomerList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /businessCard/getCustomerInfo 查询用户信息
export const getCustomerInfoApi = params => {
  return request({
      url: 'businessCard/getCustomerInfo',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /businessCard/selectCustomerListExport 导出用户列表
export const selectCustomerListExportApi = params => {
  return request({
      url: 'businessCard/selectCustomerListExport',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/selectCompanyList 查询企业列表
export const selectCompanyListApi = params => {
  return request({
      url: 'company/selectCompanyList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/selectAgentCompanyList 查询代理企业列表
export const selectAgentCompanyListApi = params => {
  return request({
      url: 'company/selectAgentCompanyList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/getAdminCompanyInfo 查询企业信息
export const getAdminCompanyInfoApi = params => {
  return request({
      url: 'company/getAdminCompanyInfo',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/addCompanyService 公司购买名片服务数
export const addCompanyServiceApi = params => {
  return request({
      url: 'company/addCompanyService',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/auditCompany 审核公司
export const auditCompanyApi = params => {
  return request({
      url: 'company/auditCompany',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// /company/selectCompanyListExport 导出企业列表
export const selectCompanyListExportApi = params => {
  return request({
      url: 'company/selectCompanyListExport',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/getTemplateDraftList 查询草稿箱
export const getTemplateDraftListApi = params => {
  return request({
      url: 'company/getTemplateDraftList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/getTemplateList 查询小程序模板库
export const getTemplateListApi = params => {
  return request({
      url: 'company/getTemplateList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/selectAdminList 查询总后台管理员列表
export const selectAdminListApi = params => {
  return request({
      url: 'company/selectAdminList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/selectMenuList 查询管理员权限列表
export const selectMenuListApi = params => {
  return request({
      url: 'company/selectMenuList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/saveAdmin 新增总/修改后台管理员
export const saveAdminApi = params => {
  return request({
      url: 'company/saveAdmin',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/inserAdminRole 设置总后台管理员权限
export const inserAdminRoleApi = params => {
  return request({
      url: 'company/inserAdminRole',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/delAdmin 删除管理员
export const delAdminApi = params => {
  return request({
      url: 'company/delAdmin',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// company/configureAgen 设置企业代理权限和代理价格
export const configureAgenApi = params => {
  return request({
      url: 'company/configureAgen',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/selectCompanyAccountList 查询公司账号列表
export const selectCompanyAccountListApi = params => {
  return request({
      url: 'Wechat/selectCompanyAccountList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/appletCodeManageList 本地小程序版本代码管理列表
export const appletCodeManageListApi = params => {
  return request({
      url: 'Wechat/appletCodeManageList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/commit  代码提交到相应小程序
export const commitApi = params => {
  return request({
      url: 'Wechat/commit',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/getQrCode 获取体验码
export const getQrCodeApi = params => {
  return request({
      url: 'Wechat/getQrCode',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/getLatestAuditStatus 获取审核结果
export const getLatestAuditStatusApi = params => {
  return request({
      url: 'Wechat/getLatestAuditStatus',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/submitAudit 提交审核接口
export const submitAuditApi = params => {
  return request({
      url: 'Wechat/submitAudit',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// Wechat/release 发布小程序
export const releaseApi = params => {
  return request({
      url: 'Wechat/release',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// index/selectAdminRecordList 查询管理员操作记录列表
export const selectAdminRecordListApi = params => {
  return request({
      url: 'index/selectAdminRecordList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// notice/selectNoticeList 查询企业公告列表
export const selectNoticeListApi = params => {
  return request({
      url: 'notice/selectNoticeList',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// notice/insertNotice 新增/编辑企业公告信息
export const insertNoticeApi = params => {
  return request({
      url: 'notice/insertNotice',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};

// notice/delNotice 删除企业公告信息
export const delNoticeApi = params => {
  return request({
      url: 'notice/delNotice',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};


export const oneLoginApi = params => {
  return request({
      url: 'home/login/company',
      transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
        data = Qs.stringify(data);
        return data
      }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      },
  });
};


export const goodsImportApi = params => {
  return request({
      url: 'goods/import',
      // transformRequest: [function(data) {      //在请求之前对data传参进行格式转换
      //   data = Qs.stringify(data);
      //   return data
      // }],
      method: 'post',
      data: params,
      headers:{
        'Content-Type': 'multipart/form-data'
      },
  });
};
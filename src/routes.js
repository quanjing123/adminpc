import Home from './views/Home.vue'

let routes = [
    {
        path: '/login',
        component: () => import('@/views/Login.vue'),
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/404.vue'),
        name: '',
        hidden: true
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-tv',
        leaf: true,//只有一个节点
        children: [
            { path: '/index', component: () => import('@/views/index/index.vue'), name: '工作台' }
        ],
        redirect: { path: '/index' }
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-user-circle',
        leaf: true,//只有一个节点
        children: [
            { path: '/customerManage', component: () => import('@/views/customerManage/index.vue'), name: '用户管理' },
            { path: '/cardPage', component: () => import('@/views/cardPage/index.vue'), name: '名片详情', hidden: true }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '企业管理',
        iconCls: 'fa fa-address-card',
        children: [
            { path: '/companyManage', component: () => import('@/views/companyManage/index.vue'), name: '企业管理' },
            { path: '/companyCardPage', component: () => import('@/views/companyCardPage/index.vue'), name: '企业详情', hidden: true },
            { path: '/equMgr', component: () => import('@/views/equMgr/index.vue'), name: '设备审核' },
            // { path: '/bannerMgr', component: () => import('@/views/bannerMgr/index.vue'), name: 'Banner管理' },
            { path: '/companySelect', component: () => import('@/views/companySelect/index.vue'), name: '优选审核' },
            { path: '/companyNotice', component: () => import('@/views/companyNotice/index.vue'), name: '企业公告' },
            { path: '/companyLicense', component: () => import('@/views/companyLicense/index.vue'), name: '执照审核' },
            { path: '/companyLicense/info', component: () => import('@/views/companyLicense/info.vue'), name: '执照-企业详情', hidden: true }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '平台管理',
        iconCls: 'fa fa-address-card',
        children: [
            { path: '/goodsManage', component: () => import('@/views/goodsManage/index.vue'), name: '商品管理' },
            { path: '/goodsType', component: () => import('@/views/goodsType/index.vue'), name: '分类管理' },
            { path: '/appConfig', component: () => import('@/views/appConfig/index.vue'), name: 'APP配置'},
            { path: '/companyPlatform', component: () => import('@/views/companyPlatform/index.vue'), name: '企业平台' },
            { path: '/dynamic', component: () => import('@/views/dynamic/index.vue'), name: '红木圈管理' },
            { path: '/live', component: () => import('@/views/live/index.vue'), name: '直播管理' }
        ]
    },
    {
        path: '/data',
        component: Home,
        name: '数据中心',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/data/yimaiSmartCard', component: () => import('@/views/yimaiSmartCard/index.vue'), name: '红木名片智能名片' },
            { path: '/data/yimaiCrmCard', component: () => import('@/views/yimaiCrmCard/index.vue'), name: '红木名片智能CRM' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '小程序管理',
        iconCls: 'fa fa-file-text',
        children: [
            { path: '/codeManage', component: () => import('@/views/codeManage/index.vue'), name: '代码管理' },
            { path: '/versionManage', component: () => import('@/views/versionManage/index.vue'), name: '小程序版本管理' },
            { path: '/wxappletManage', component: () => import('@/views/wxappletManage/index.vue'), name: '小程序发布' }
        ]
    // },    {
    //     path: '/',
    //     component: Home,
    //     name: '供应与代理',
    //     iconCls: 'fa fa-handshake-o',
    //     children: [
    //         { path: '/companyApply/supply', component: () => import('@/views/companyApply/supply.vue'), name: '供应商管理' },
    //         { path: '/companyApply/agent', component: () => import('@/views/companyApply/agent.vue'), name: '代理商管理' },
    //         { path: '/companyCategory', component: () => import('@/views/companyCategory/index.vue'), name: '类目管理' }
    //     ]
    },
    {
        path: '/',
        component: Home,
        name: '广告推送',
        iconCls: 'fa fa-cog',
        children: [
            { path: '/templateMgr', component: () => import('@/views/templateMgr/index.vue'), name: '获客海报' },
            { path: '/templateEdit', component: () => import('@/views/templateMgr/edit.vue'), name: '海报编辑', hidden: true },
            { path: '/articleMgr', component: () => import('@/views/articleMgr/index.vue'), name: '红木头条' },
            { path: '/articleEdit', component: () => import('@/views/articleMgr/edit.vue'), name: '新增头条', hidden: true },
            { path: '/groupBuying', component: () => import('@/views/groupBuying/index.vue'), name: '拼团管理' },
            { path: '/groupBuying/edit', component: () => import('@/views/groupBuying/edit.vue'), name: '拼团新增', hidden: true }
        ],
        redirect: { path: '/index' }
    },
    {
        path: '/',
        component: Home,
        name: '系统管理',
        iconCls: 'fa fa-cog',
        children: [
            { path: '/permissionsManage', component: () => import('@/views/permissionsManage/index.vue'), name: '权限管理' },
            { path: '/operationRecords', component: () => import('@/views/operationRecords/index.vue'), name: '操作记录' },
        ],
        redirect: { path: '/index' }
    },
    {
        path: '/',
        component: Home,
        name: '奖券系统',
        iconCls: 'fa fa-cog',
        children: [
            { path: '/ticketOrder', component: () => import('@/views/ticketOrder/index.vue'), name: '奖券订单' },
            { path: '/ticket', component: () => import('@/views/ticket/index.vue'), name: '奖券管理' },
            { path: '/ticket/detail', component: () => import('@/views/ticket/detail.vue'), name: '订单明细', hidden: true },
            { path: '/ticketQr', component: () => import('@/views/ticket/indexQr.vue'), name: '二维码核销' },
            { path: '/ticketCompany', component: () => import('@/views/ticketCompany/index.vue'), name: '订单统计' },
            { path: '/ticketCompany/detail', component: () => import('@/views/ticketCompany/detail.vue'), name: '订单统计-明细', hidden: true },
        ],
        redirect: { path: '/index' }
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
]

export  default routes

// export const constantRouterMap = [
//     {
//         path: '/', 
//         redirect: '/login'
//     },
//     {
//         path: '/login',
//         component: () => import('@/views/Login.vue'),
//         name: '',
//         hidden: true
//     },
//     {
//         path: '/404',
//         component: () => import('@/views/404.vue'),
//         name: '',
//         hidden: true
//     },
// ]

// export const asyncRouterMap = [

// ]
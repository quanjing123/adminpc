import {Loading} from 'element-ui'

let loading

function showLoad () {
  loading = Loading.service({
    lock: true,
    text: '加载中……'
  })
}

function hideLoad () {
  loading.close()
}

export {showLoad, hideLoad}

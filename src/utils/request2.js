import axios from 'axios';
import {
  Message
} from 'element-ui'
import {
  getCookie
} from '@/utils/util.js';
import router from '@/router'

/**
 * axios实例
 */
const request = axios.create({
  baseURL: process.env.ONE_baseURL,
  timeout: 10000,
  headers: {

  }
});

/**
 * 请求前添加token
 */
request.interceptors.request.use(function (config) {
  var token = getCookie('token');
  config.headers['token'] = token !== '' ? token : ''; // 获取token 也就是说在设置的时候也要设置成token
  return config;
});

/**
 * 请求回来处理数据
 */
request.interceptors.response.use(function (res) {
  if (res.config.url.indexOf('getQrCode') !== -1) {
    return res.request.response
  }
  if (res.config.url.indexOf('/company/suncode') !== -1) {
    return res.request.response
  }

  //==============  下载处理  ====================
  if (res.config.xconfig.isDownload) {
    let link = document.createElement('a');
    let url = window.URL.createObjectURL(res.data);
    link.href = url;
    link.download = res.headers['x-download-file-name'] ? decodeURI(res.headers['x-download-file-name']) : '导出文件.xls';

    link.click();
    window.URL.revokeObjectURL

    // return res
    return res;
  }

  //==============  业务成功  ====================
  var result = JSON.parse(res.request.response);
  if ((result.code === 200 || result.code === 201) || result.errcode === 0) {
    if (res.config.xconfig.isSuccessHandler) { // 成功处理
      Message({
        message: "操作成功",
        type: "success"
      });
      setTimeout(() => {
        return Promise.resolve(result.data)
      }, 100);
    } else {
      return result.data;
    }
  } else {

    //==============  业务错误处理  ====================
    Message({
      message: result.message ? result.message : result.errmsg,
      type: 'warning'
    });
    if (result.code === 401) { //需要重新登录
      router.push({
        path: '/login'
      })
    }
    console.log(result)
    return Promise.reject(result);
  }
}, err => {

  //==============  HTTP错误处理  ====================
  if (err && err.response) {
    switch (err.response.status) {
      case 400:
        err.message = '请求错误(400)';
        break;
      case 401:
        err.message = '未授权，请重新登录(401)';
        break;
      case 403:
        err.message = '拒绝访问(403)';
        break;
      case 404:
        err.message = '请求出错(404)';
        break;
      case 408:
        err.message = '请求超时(408)';
        break;
      case 500:
        err.message = '服务器错误(500)';
        break;
      case 501:
        err.message = '服务未实现(501)';
        break;
      case 502:
        err.message = '网络错误(502)';
        break;
      case 503:
        err.message = '服务不可用(503)';
        break;
      case 504:
        err.message = '网络超时(504)';
        break;
      case 505:
        err.message = 'HTTP版本不受支持(505)';
        break;
      default:
        err.message = `连接出错(${err.response.status})!`;
    }
  } else {
    err.message = '连接服务器失败!'
  }
  Message({
    message: err.message,
    type: 'warning'
  });
  return Promise.resolve(err);
});

// function successHandler(res) {
//   Message({
//     message: "操作成功",
//     type: "success"
//   });
//   setTimeout(() => {
//     Promise.resolve(res)
//   }, 100);
// }

// export {
//   request,
//   successHandler
// };

export function get(url, params, xconfig) {
  if (!xconfig) {
    xconfig = {}
  }
  var p1 = request({
    url: url,
    method: 'get',
    params: params,
    responseType: xconfig.isDownload ? 'blob' : '',
    xconfig: {
      isDownload: xconfig.isDownload
    }
  });
  // if (xconfig.isDownload) {
  //   p1.then(function successHandler(res) {
  //     let link = document.createElement('a');
  //     let url = window.URL.createObjectURL(res.data);
  //     link.href = url;
  //     link.download = res.headers['x-download-file-name'] ? decodeURI(res.headers['x-download-file-name']) : '导出文件.xls';

  //     link.click();
  //     window.URL.revokeObjectURL
  //   });
  // }
  return p1;
}
export function post(url, params, isSuccessHandler) {
  var p1 = request({
    url: url,
    method: 'post',
    data: params,
    xconfig: {
      isSuccessHandler: isSuccessHandler
    }
  })
  // if (isSuccessHandler) {
  // p1.then(successHandler());
  // }
  return p1;
}
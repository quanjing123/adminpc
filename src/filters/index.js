
function formatDate (str) {
    if (!str || str == '') {
        return ''
    }
    let d = new Date(str)
    let time = d.getFullYear() + "-" + (d.getMonth() + 1).toString().padStart(2,'0') + "-" + d.getDate().toString().padStart(2,'0');
    return time
}

function formatDateTime (str) {
    if (!str || str == '') {
        return ''
    }
    let d = new Date(str)
    let time = d.getFullYear() + "-" + (d.getMonth() + 1).toString().padStart(2,'0') + "-" + d.getDate().toString().padStart(2,'0') + " " + d.getHours().toString().padStart(2,'0') + ":" + d.getMinutes().toString().padStart(2,'0') + ":" + d.getSeconds().toString().padStart(2,'0');
    return time
}

function formatDateMin (str) {
    if (!str || str == '') {
        return ''
    }
    let d = new Date(str)
    let time = d.getFullYear() + "-" + (d.getMonth() + 1).toString().padStart(2,'0') + "-" + d.getDate().toString().padStart(2,'0') + " " + d.getHours().toString().padStart(2,'0') + ":" + d.getMinutes().toString().padStart(2,'0');
    return time
}

function getStatus (companyCard) {
    if (companyCard.endTime) {
        let endTime = new Date(companyCard.endTime).getTime();
        let nowTime = new Date().getTime();
        if (endTime > nowTime) {
            return "" // 未到期返回''
        } else {
            return "已到期"
        }
    } else {
        return "未授权"
    }
}

function timestampToMin (timestamp) {
    let time = Number(String(timestamp * 1000).slice(0, 13))
    return formatDateMin(new Date(time))
}

function getStatusByTimestamp(timestamp) {
    let nowTime = new Date().getTime();
    if (timestamp > nowTime) {
        return "已授权"
    } else {
        return "已到期"
    }
}

function getState(state) {
    if (typeof state == 'undefined'){
        return '暂无'
    }
    let status = {
        '-1': '提交失败',
        1: '提交成功',
        2: '审核中',
        3: '审核提交失败',
        4: '审核失败',
        5: '审核成功',
        6: '发布成功',
        7: '发布失败',
    }
    return status[state]
}

export default { formatDate, formatDateTime, formatDateMin, getStatus, timestampToMin, getStatusByTimestamp, getState }
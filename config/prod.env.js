module.exports = {
  NODE_ENV: '"production"',
  ONE_baseURL: '"https://qjapi.hongmushichang.com/adminApi/"', // 正式环境
  ONE_uploadURL: '"https://qjapi.hongmushichang.com/file/upload"',
  ONE_uploadEncodeURL: '"https://qjapi.hongmushichang.com/file/upload/encode"',
  ONE_WEB_TITLE: '"红木名片总后台"',
  ONE_VERSION: '"1.6.9"',
}
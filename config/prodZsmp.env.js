module.exports = {
  NODE_ENV: '"production"',
  ONE_baseURL: '"https://api.jiajushilian.com/adminApi/"', // 正式环境
  ONE_uploadURL: '"https://api.jiajushilian.com/file/upload"',
  ONE_uploadEncodeURL: '"https://api.jiajushilian.com/file/upload/encode"',
  ONE_WEB_TITLE: '"红木名片总后台"',
}

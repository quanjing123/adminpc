module.exports = {
  NODE_ENV: '"production"',
  ONE_baseURL: '"https://api.jincard.cn/adminApi/"', // 正式环境
  ONE_uploadURL: '"https://api.jincard.cn/file/upload"',
  ONE_uploadEncodeURL: '"https://api.jincard.cn/file/upload/encode"',
  ONE_WEB_TITLE: '"红木名片总后台"',
}

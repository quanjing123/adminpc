var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // ONE_baseURL: '"http://192.168.1.92:8085/adminApi/"', // 正式环境
  // ONE_uploadURL: '"http://192.168.1.92:8085/file/upload"',
  // ONE_uploadEncodeURL: '"http://192.168.1.92:8085/file/upload/encode"',
})

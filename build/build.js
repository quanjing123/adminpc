require('./check-versions')()

console.log('Running '+process.env.npm_lifecycle_event+'...')
// console.log(JSON.parse(process.env.npm_config_argv))

process.env.NODE_ENV = 'production'
// process.env.NODE_CONFIG = JSON.parse(process.env.npm_config_argv).remain[0]
process.env.NODE_CONFIG = process.env.npm_lifecycle_event

var ora = require('ora')
var rm = require('rimraf')
var path = require('path')
var chalk = require('chalk')
var webpack = require('webpack')
var config = require('../config')
var webpackConfig = require('./webpack.'+process.env.NODE_CONFIG+'.conf')

var spinner = ora('building for '+process.env.NODE_ENV+'/'+process.env.NODE_CONFIG+'...')
spinner.start()

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
    console.log(process.env.NODE_ENV+'/'+process.env.NODE_CONFIG)
  })
})
